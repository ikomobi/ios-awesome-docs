# Xcode 8 migration guide

## Xcode 8 & Swift 2.3

Make a project in **Xcode 8 & Swift 2.3** from **Xcode 7 & Swift 2.2**

- Create an **feature/iOS10** branch
- Make the `Use Legacy Swift Language Version` to `Yes` in **Build Settings**.
- Define the pod lib version according to the [Dependencies compatibility](#dependencies-compatibility) section
- Add a post install hook to the `Podfile` :

```
post_install do |installer|
    installer.pods_project.build_configurations.each do |config|
        # Configure Pod targets for Xcode 8 compatibility
        config.build_settings['SWIFT_VERSION'] = '2.3'
        config.build_settings['PROVISIONING_PROFILE_SPECIFIER'] = 'com.ikomobi/'
        config.build_settings['ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES'] = 'NO'
    end
end
```

- Make the minors changes observed (with compil errors) by Xcode

Some tips in this blog post : [Simultaneous Xcode 7 and Xcode 8 compatibility](http://radex.io/xcode7-xcode8/)

## Xcode 8 & Swift 3.0

- To do

## Dependencies compatibility

### Alamofire - [Github](https://github.com/Alamofire/Alamofire) ![Swift 2.3.x](https://img.shields.io/badge/Swift-2.3.x-green.svg) ![Swift 3.0.x](https://img.shields.io/badge/Swift-3.0.x-red.svg)
- Use `swift3` branch for Swift 3 compatibility, she doesn't work at this time. /!\ iOS 9 minimum
- Use `swift2.3` branch for Swift 2.3 compatibility. Like :

```
	pod 'Alamofire', :git => 'https://github.com/Alamofire/Alamofire.git', :branch => 'swift2.3'
```

### Charts - [Github](https://github.com/danielgindi/Charts) ![Swift 2.3.x](https://img.shields.io/badge/Swift-2.3.x-green.svg) ![Swift 3.0.x](https://img.shields.io/badge/Swift-3.0.x-green.svg)
- Already compatible with Swift 3 on last release.
- Use `Swift-2.3` branch for Swift 2.3 compatibility.

### ReactiveCocoa - [Github](https://github.com/ReactiveCocoa/ReactiveCocoa) ![Swift 2.3.x](https://img.shields.io/badge/Swift-2.3.x-green.svg) ![Swift 3.0.x](https://img.shields.io/badge/Swift-3.0.x-red.svg)
- Last version `v4.2.2` is compatible with Swift 2.3
- Use `master` branch for Swift 3 compatibility. -> *Under developpement and not possible for the moment because `.podspec` is missing from `master` branch.*

```
   pod "ReactiveCocoa", :git => 'https://github.com/ReactiveCocoa/ReactiveCocoa.git', :branch => 'master'
```

### ObjectMapper - [Github](https://github.com/Hearst-DD/ObjectMapper) ![Swift 2.3.x](https://img.shields.io/badge/Swift-2.3.x-green.svg)
- Swift 2.3 compatible
- Use `swift-3` branch for Swift 3 compatibility. -> *Under developpement and don't work*

### Parse - [Github](https://github.com/ParsePlatform/Parse-SDK-iOS-OSX) ![Language Objective-C](https://img.shields.io/badge/Language-ObjectiveC-blue.svg) ![Swift 2.3.x](https://img.shields.io/badge/Swift-2.3.x-green.svg) ![Swift 3.0.x](https://img.shields.io/badge/Swift-3.0.x-green.svg)
- Compatible

### SSZipArchive - [Github](https://github.com/ZipArchive/ZipArchive) ![Swift 2.3.x](https://img.shields.io/badge/Swift-2.3.x-green.svg) ![Swift 3.0.x](https://img.shields.io/badge/Swift-3.0.x-red.svg)
- Swift 2.3 compatible
- Swift 3 seems compatible but not tested

### ReachabilitySwift - [Github](https://github.com/ashleymills/Reachability.swift) ![Swift 2.3.x](https://img.shields.io/badge/Swift-2.3.x-green.svg) ![Swift 3.0.x](https://img.shields.io/badge/Swift-3.0.x-red.svg)
- Swift 2.3 compatible.
- Swift 3 support under developpement in `feature/ios10` branch.

