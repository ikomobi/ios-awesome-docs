# Awesome docs for iOS team

- [Xcode 8 Migration Guide](xcode-8-migration-guide.md)
- [Swift Style Guide](swiftStyle.md)
- [ObjectiveC Style Guide](objectiveCStyle.md)
- [CoreData inclusion in existing projects Guide](coreDataInclusion.md)
